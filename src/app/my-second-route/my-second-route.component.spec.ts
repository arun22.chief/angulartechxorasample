import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MySecondRouteComponent } from './my-second-route.component';

describe('MySecondRouteComponent', () => {
  let component: MySecondRouteComponent;
  let fixture: ComponentFixture<MySecondRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MySecondRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MySecondRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
