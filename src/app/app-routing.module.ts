import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HospitalDetailsComponent } from './hospital-details/hospital-details.component';
import { MySecondRouteComponent } from './my-second-route/my-second-route.component';

const routes: Routes = [
  { path: 'MyFirstRoute', component: HospitalDetailsComponent },
  { path: 'MySecondRoute', component: MySecondRouteComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
