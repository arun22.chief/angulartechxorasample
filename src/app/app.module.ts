import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MySecondComponentComponent } from './my-second-component/my-second-component.component';
import { AdminContainerComponent } from './admin-container/admin-container.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminFooterComponent } from './admin-footer/admin-footer.component';
import { HospitalDetailsComponent } from './hospital-details/hospital-details.component';
import { MySecondRouteComponent } from './my-second-route/my-second-route.component';

@NgModule({
  declarations: [
    AppComponent,
    MySecondComponentComponent,
    AdminContainerComponent,
    AdminHeaderComponent,
    AdminFooterComponent,
    HospitalDetailsComponent,
    MySecondRouteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
